I created this module in a effort to reduce time spent on setting up a method 
that allows other websites and/or mobile systems 
to communicate with drupal websites. 
I hope I can help other people making this easier.

The module allows to connect your own function to methodes, 
that can then be called through the JSON-RPC connection. 
A user can only make a connection if he has a valid user account 
and the wright role that is connected to the service.

The modules allows other modules to easily extend on methodes, 
so developer can focus on writing their modules that need to cooperate 
with the JSON-RPC methodes. An module is included that allows to view, load, 
save and delete nodes. Use this as an example on how to extend the module.

Warning: Please be carefull when using this module, 
if you give a JSON-RPC client to much rights he can 
damage or overtake your website, use the tool wisely.
